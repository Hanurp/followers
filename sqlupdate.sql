CREATE TABLE "man_man_follow" (
    "id" integer NOT NULL PRIMARY KEY,
    "from_man_id" integer NOT NULL,
    "to_man_id" integer NOT NULL,
    UNIQUE ("from_man_id", "to_man_id")
)
;