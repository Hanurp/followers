# -*- coding: utf-8 -*-
# django
from django.test import TestCase
from django.core import management
# local
from .models import Man


class ManManagementTests(TestCase):
    def setUp(self):
        self.m1 = Man.objects.create(name=u'Man1', follow_ids=u'3')
        self.m2 = Man.objects.create(name=u'Man2', follow_ids=u'3 1')
        self.m3 = Man.objects.create(name=u'Man3', follow_ids=u'1')

    def test_man_management_fill_followers_from_txt_pursued(self):
        # act
        #  запустим комманду заполнения таблицы followers
        management.call_command('fill_followers_from_txt')
        # assert
        expected = 2  # ожидаем получить два преследователя за m1
        received = self.m1.pursued.all().count()
        self.assertEqual(expected, received)

    def test_man_management_fill_followers_from_txt_follower(self):
        # act
        #  запустим комманду заполнения таблицы followers
        management.call_command('fill_followers_from_txt')
        # assert
        expected = 1  # ожидаем что m1 преследует только одного человека
        received = self.m1.follow.all().count()
        self.assertEqual(expected, received)


class ManMethodTests(TestCase):
    def setUp(self):
        self.m1 = Man.objects.create(name=u'Man1', follow_ids=u'3')
        self.m2 = Man.objects.create(name=u'Man2', follow_ids=u'3 1')
        self.m3 = Man.objects.create(name=u'Man3', follow_ids=u'1')
        # add m2m
        self.m1.follow = [self.m3]
        self.m1.save()
        self.m2.follow = [self.m3, self.m1]
        self.m2.save()
        self.m3.follow = [self.m1]
        self.m3.save()

    def test_man_save_follow_amount_init(self):
        # act
        # assert
        expected = 1  # проверим изначальное заполнение
        received = self.m1.amounts.follow_amount
        self.assertEqual(expected, received)

    def test_man_save_follow_amount_change(self):
        # act
        self.m1.follow = [self.m2, self.m3]
        self.m1.save()
        # assert
        expected = 2  # ожидаем двух преследуемых
        received = self.m1.amounts.follow_amount
        self.assertEqual(expected, received)

    def test_man_save_follow_pursuing_init(self):
        # act
        # assert
        expected = 2  # проверим изначальное заполнение
        received = self.m1.amounts.pursuing_amount
        self.assertEqual(expected, received)

    def test_man_save_follow_pursuing_change(self):
        # act
        self.m1.follow = [self.m2, self.m3]
        self.m1.save()
        # assert
        expected = 2  # ожидаем двух преследователей
        received = self.m1.amounts.pursuing_amount
        self.assertEqual(expected, received)

    def test_man_save_get_follow_amount(self):
        # act
        # assert
        expected = 1
        received = self.m1.get_follow_amount()
        self.assertEqual(expected, received)

    def test_man_save_get_follow_pursuing(self):
        # act
        # assert
        expected = 2
        received = self.m1.get_pursuing_amount()
        self.assertEqual(expected, received)
