# -*- coding: utf-8 -*-
# python
from optparse import make_option
# django
from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError
# followers
from man.models import Man


class Command(BaseCommand):
    def handle(self, *args, **options):
        all_mans = Man.objects.all()
        # переберем всех людей
        for man in all_mans:
            if not man.follow_ids:
                continue  # пропускаем если пустой список преследуемых

            man.follow = man.follow_ids.split(' ')
            man.save()
            self.stdout.write('.')
        self.stdout.write('Filling completed.\n')