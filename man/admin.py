# coding: utf-8
# django
from django.contrib import admin
# local
from .models import Man


class PursuedInline(admin.TabularInline):
    """
    Описание инлайна преследователей
    """
    model = Man.follow.through
    fk_name = 'from_man'
    extra = 1


class ManAdmin(admin.ModelAdmin):
    list_display = ['name', 'get_follow_amount', 'get_pursuing_amount']
    search_fields = ['name']
    inlines = [
        PursuedInline
    ]
    filter_horizontal = ['follow']
    readonly_fields = ['follow_ids']
admin.site.register(Man, ManAdmin)

