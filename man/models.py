# -*- coding: utf-8 -*-
# django
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Man(models.Model):
    name = models.CharField(u'Имя', max_length=35)
    follow_ids = models.TextField(u'ID преследуемых', help_text=u'Список id преследуемых пользователей')
    follow = models.ManyToManyField('self', symmetrical=False, related_name='pursued')

    def get_follow_amount(self):
        """
        Возвращает количество преследуемых людей
        """
        return self.amounts.follow_amount

    def get_pursuing_amount(self):
        """
        Возвращает количество преследующих людей
        """
        return self.amounts.pursuing_amount

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)


@receiver(post_save, sender=Man)
def man_post_save(sender, **kwargs):
        """
        Сигнал выполняемые после сохранения объекта модели Man, необходимые для вычисления и обновления количества
        преследуемых и преследователей
        """
        man = kwargs.get('instance')
        if not man:
            return

        amount, create = Amounts.objects.get_or_create(man=man)
        follows = man.follow.all()
        amount.follow_amount = follows.count()  # вычислим количество преследуемых
        amount.pursuing_amount = man.pursued.all().count()  # вычислим количество преследуемых
        amount.save()

        # сохраним информацию о преследуемых в текстовое поле
        man.follow_ids = follows.values_list('pk', flat=True)

        # обновим о количестве преследователей у всех преследуемых
        for pursued in man.follow.all():
            amount, create = Amounts.objects.get_or_create(man=pursued)
            amount.pursuing_amount = pursued.pursued.all().count()
            amount.save()

        # обновим о количестве преследуемых у всех преследователей
        for follow in man.pursued.all():
            amount, create = Amounts.objects.get_or_create(man=follow)
            follows = follow.follow.all()
            amount.follow_amount = follows.count()
            amount.save()

            # сохраним информацию о преследуемых в текстовое поле
            follow.follow_ids = follows.values_list('pk', flat=True)


class Amounts(models.Model):
    """
    Модель хранит информацию о количесмтве преследуемых и преследователей
    """
    man = models.OneToOneField(Man)
    follow_amount = models.PositiveSmallIntegerField(u'Количество преследуемых', default=0)
    pursuing_amount = models.PositiveSmallIntegerField(u'Количество преследующих', default=0)


